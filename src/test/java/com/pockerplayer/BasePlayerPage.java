package com.pockerplayer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

class BasePlayerPage extends BasePage {

    @FindBy(id = "us_email")
    private WebElement f_email;

    @FindBy(id = "us_fname")
    private WebElement f_fname;

    @FindBy(id = "us_lname")
    private WebElement f_lname;

    @FindBy(id = "us_city")
    private WebElement f_city;

    @FindBy(id = "us_address")
    private WebElement f_address;

    @FindBy(id = "us_phone")
    private WebElement f_phone;

    @FindBy(id = "Submit")
    private WebElement btn_Submit;

    @FindBy(id = "Cancel")
    private WebElement btn_Cancel;

    BasePlayerPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }


    void submit(){
        btn_Submit.click();
    }

    void cancel() {
        btn_Cancel.click();
        driver.switchTo().alert().accept();
    }

    String getEmail(){
        return f_email.getAttribute("value");
    }

    String getFirstName(){
        return f_fname.getAttribute("value");
    }

    String getLastName(){
        return f_lname.getAttribute("value");
    }

    String getCity(){
        return f_city.getAttribute("value");
    }

    String getAddress(){
        return f_address.getAttribute("value");
    }

    String getPhone(){
        return f_phone.getAttribute("value");
    }

    void setEmail(String email) {
        f_email.clear();
        f_email.sendKeys(email);
    }

    void setFirstName(String firstName) {
        f_fname.clear();
        f_fname.sendKeys(firstName);
    }

    void setLastName(String lastName) {
        f_lname.clear();
        f_lname.sendKeys(lastName);
    }

    void setCity(String city) {
        f_city.clear();
        f_city.sendKeys(city);
    }

    void setAddress(String address) {
        f_address.clear();
        f_address.sendKeys(address);
    }

    void setPhone(String phone) {
        f_phone.clear();
        f_phone.sendKeys(phone);
    }
}
