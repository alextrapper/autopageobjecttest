package com.pockerplayer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InsertPlayerPage extends BasePlayerPage {

    static final String TITLE = "Players - Insert";

    @FindBy(id = "us_login")
    private WebElement f_login;

    @FindBy(id = "us_password")
    private WebElement f_password;

    @FindBy(id = "confirm_password")
    private WebElement f_confirmPassword;

    InsertPlayerPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    PlayersListPage fillFieldsAndSubmit(PockerPlayer player){
        f_login.sendKeys(player.getUserName());
        f_password.sendKeys(player.getPassword());
        f_confirmPassword.sendKeys(player.getPassword());
        setEmail(player.getEmail());
        setFirstName(player.getFirstName());
        setLastName(player.getLastName());
        setCity(player.getCity());
        setAddress(player.getAddress());
        setPhone(player.getPhone());
        submit();
        wait.until(ExpectedConditions.titleIs(PlayersListPage.TITLE));
        PlayersListPage playersListPage = new PlayersListPage(driver, wait);
        playersListPage.waitProcessing();
        return playersListPage;
    }
}
