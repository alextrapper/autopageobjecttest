package com.pockerplayer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditPlayerPage extends BasePlayerPage {
    static final String TITLE = "Players - Edit";

    EditPlayerPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    PlayersListPage editPlayer(PockerPlayer player){
        setEmail(player.getEmail());
        setFirstName(player.getFirstName());
        setLastName(player.getLastName());
        setCity(player.getCity());
        setAddress(player.getAddress());
        setPhone(player.getPhone());
        submit();
        wait.until(ExpectedConditions.titleIs(PlayersListPage.TITLE));
        PlayersListPage playersListPage = new PlayersListPage(driver, wait);
        playersListPage.waitProcessing();
        return playersListPage;
    }


}
