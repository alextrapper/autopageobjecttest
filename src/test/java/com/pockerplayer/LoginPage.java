package com.pockerplayer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

class LoginPage extends BasePage {

    static final String TITLE = "Login";

    @FindBy(id = "username")
    private WebElement f_UserName;

    @FindBy(id = "password")
    private WebElement f_Password;

    @FindBy(id = "logIn")
    private WebElement btn_Login;

    LoginPage(WebDriver driver, WebDriverWait wait){
        super(driver, wait);
    }

    PlayersListPage successfullLogin(String userName, String password){
        wait.until(ExpectedConditions.titleIs(LoginPage.TITLE));
        wait.until(ExpectedConditions.elementToBeClickable(btn_Login));
        f_UserName.clear();
        f_UserName.sendKeys(userName);
        f_Password.clear();
        f_Password.sendKeys(password);
        btn_Login.click();
        wait.until(ExpectedConditions.titleIs(PlayersListPage.TITLE));
        return new PlayersListPage(driver, wait);
    }

    LoginPage unSuccessfullLogin(String userName, String password){
        wait.until(ExpectedConditions.titleIs(LoginPage.TITLE));
        wait.until(ExpectedConditions.elementToBeClickable(btn_Login));
        f_UserName.sendKeys(userName);
        f_Password.sendKeys(password);
        btn_Login.click();
        return this;
    }

    BasePage login (String userName, String password){
        f_UserName.sendKeys(userName);
        f_Password.sendKeys(password);
        btn_Login.click();
        return new BasePage(driver, wait);
    }

}
