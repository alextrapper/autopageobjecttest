package com.pockerplayer;

import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;


public class EditPlayerTest extends BaseTest {
    private PlayersListPage playersListPage;
    private EditPlayerPage editPlayerPage;
    private PockerPlayer p1;
    private PockerPlayer p2;



    @BeforeMethod
    private void preconditions(){
        System.out.println("executing Method com.pockerplayer.EditPlayerTest.preconditions() @BeforeMethod");
        p1 = TestData.getPlayers().get(0);
        p2 = TestData.getPlayers().get(1);
        playersListPage = new LoginPage(driver, wait).successfullLogin(TestData.adminLogin, TestData.adminPassword)
                .getInsertPlayerPage().fillFieldsAndSubmit(p1);
    }

    @Test
    public void editExistingPlayerTest(){
        System.out.println("executing Method com.pockerplayer.EditPlayerTest.editExistingPlayerTest() @Test");
        editPlayerPage = playersListPage.getEditPlayerPage(p1).editPlayer(p2).getEditPlayerPage(p1);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(editPlayerPage.getEmail(),p2.getEmail());
        softAssert.assertEquals(editPlayerPage.getFirstName(),p2.getFirstName());
        softAssert.assertEquals(editPlayerPage.getLastName(),p2.getLastName());
        softAssert.assertEquals(editPlayerPage.getCity(),p2.getCity());
        softAssert.assertEquals(editPlayerPage.getAddress(),p2.getAddress());
        softAssert.assertEquals(editPlayerPage.getPhone(),p2.getPhone());
        softAssert.assertAll();
        editPlayerPage.cancel();
    }


    @AfterMethod
    private void deletePlayer(){
        System.out.println("executing Method com.pockerplayer.EditPlayerTest.deletePlayer() @AfterMethod");
        playersListPage.deletePlayer(p1);
    }

    @AfterClass
    private void logOut(){
        System.out.println("executing Method com.pockerplayer.EditPlayerTest.logOut() @AfterClass");
        playersListPage.logout();
    }



}
