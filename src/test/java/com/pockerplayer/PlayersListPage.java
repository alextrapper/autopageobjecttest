package com.pockerplayer;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

class PlayersListPage extends BasePage {

    static final String TITLE = "Players";

    @FindBy(xpath = "//a[contains(@href, 'logout')]")
    private WebElement btn_Logout;

    @FindBy(xpath = "//a[@href='/players/insert']")
    private WebElement btn_InsertPlayer;

    @FindBy (xpath = "//button[@type='submit']")
    private WebElement btn_Search;

    @FindBy(xpath = "//button[@type='reset']")
    private WebElement btn_Reset;

    @FindBy(id = "login")
    private WebElement f_searchLogin;

    private WebElement btn_EditPlayer;
    private WebElement btn_DeletePlayer;

    @FindBy(id = "grid_processing")
    private WebElement alert_processing;

    private By locator_UserListSearchResult = By.xpath("//a[contains(@href, '/players/edit/popup')]");

    PlayersListPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    InsertPlayerPage getInsertPlayerPage(){
        btn_InsertPlayer.click();
        return new InsertPlayerPage(driver, wait);
    }

    EditPlayerPage getEditPlayerPage(PockerPlayer player){
        wait.until(ExpectedConditions.invisibilityOf(alert_processing));
        searchPlayer(player);
        driver.findElement(By.xpath(getBtnEditXPath(player))).click();
        wait.until(ExpectedConditions.titleIs(EditPlayerPage.TITLE));
        return new EditPlayerPage(driver, wait);
    }

    PlayersListPage deletePlayer(PockerPlayer player){
        searchPlayer((player));
        wait.until(ExpectedConditions.elementToBeClickable(
                driver.findElement(By.xpath(getBtnDeleteXPath(player)))));
        driver.findElement(By.xpath(getBtnDeleteXPath(player))).click();
        driver.switchTo().alert().accept();
        return this;
    }

    List<WebElement> getSearchResults(PockerPlayer player){
        searchPlayer(player);
        waitProcessing();
        return driver.findElements(locator_UserListSearchResult);
    }

    private String getBtnEditXPath (PockerPlayer player){
        return "//a[text()='" + player.getUserName() + "']//..//..//a[@title='Edit']";
    }

    private String getBtnDeleteXPath (PockerPlayer player) {
        return "(//a[text()='" + player.getUserName() +  "']//..//..)//a[@title='Delete']";
    }

    private void searchPlayer(PockerPlayer player){
        wait.until(ExpectedConditions.titleIs(TITLE));
        btn_Reset.click();
        wait.until(ExpectedConditions.elementToBeClickable(f_searchLogin));
        f_searchLogin.clear();
        f_searchLogin.sendKeys(player.getUserName());
        btn_Search.click();
        waitProcessing();
    }

    void getPlayersSearchList (PockerPlayer player){
        f_searchLogin.clear();
        f_searchLogin.sendKeys(player.getUserName());
        btn_Search.click();
        waitProcessing();
    }

    void logout(){
        btn_Logout.click();
    }

    void waitProcessing(){
        wait.until(ExpectedConditions.invisibilityOf(alert_processing));
    }
}
