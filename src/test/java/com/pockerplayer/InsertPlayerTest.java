package com.pockerplayer;

import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

public class InsertPlayerTest extends BaseTest {
    private BasePage page;
    private PlayersListPage playersListPage;
    private EditPlayerPage editPlayerPage;
    private PockerPlayer player;


    @BeforeMethod
    private void initPage(){
        System.out.println("executing Method com.pockerplayer.InsertPlayerTest.initPage() @BeforeMethod");
        LoginPage loginPage = new LoginPage(driver, wait);
        playersListPage = loginPage.successfullLogin(TestData.adminLogin, TestData.adminPassword);
        player = TestData.getPlayers().get(0);

    }

    @Test
    public void insertingPlayerTest(){
        System.out.println("executing Method com.pockerplayer.InsertPlayerTest.insertingPlayerTest() @Test");
        page = playersListPage.getInsertPlayerPage().fillFieldsAndSubmit(player);
        Assert.assertEquals(page.getTitle(), PlayersListPage.TITLE);
        playersListPage = (PlayersListPage) page;
        editPlayerPage = playersListPage.getEditPlayerPage(player);


        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(editPlayerPage.getEmail(),player.getEmail());
        softAssert.assertEquals(editPlayerPage.getFirstName(),player.getFirstName());
        softAssert.assertEquals(editPlayerPage.getLastName(),player.getLastName());
        softAssert.assertEquals(editPlayerPage.getCity(),player.getCity());
        softAssert.assertEquals(editPlayerPage.getAddress(),player.getAddress());
        softAssert.assertEquals(editPlayerPage.getPhone(),player.getPhone());
        softAssert.assertAll();

        editPlayerPage.cancel();
    }

    @AfterMethod
    private void deletePlayer(){
        System.out.println("executing Method com.pockerplayer.InsertPlayerTest.deletePlayer() @AfterMethod");
        playersListPage.deletePlayer(player);
    }

    @AfterClass
    private void logOut(){
        System.out.println("executing Method com.pockerplayer.InsertPlayerTest.logOut() @AfterClass");
        playersListPage.logout();
    }



}
