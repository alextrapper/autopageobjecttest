package com.pockerplayer;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    private LoginPage loginPage;
    private BasePage page;


    @BeforeClass
    void initPages(){
        loginPage = new LoginPage(driver, wait);
    }

    @Test
    void successLogin(){
        page = loginPage.successfullLogin(TestData.adminLogin,TestData.adminPassword);
        Assert.assertEquals(page.getTitle(),PlayersListPage.TITLE);
        PlayersListPage playersListPage = (PlayersListPage) page;
        playersListPage.logout();
    }

    @Test
    void unSuccessLogin(){
        page = loginPage.unSuccessfullLogin(TestData.adminLoginWrong, TestData.adminPasswordWrong);
        Assert.assertEquals(page.getTitle(), LoginPage.TITLE);
    }
}
