package com.pockerplayer;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

public class BaseTest {
    WebDriver driver;
    WebDriverWait wait;


    @BeforeClass
    public void setupDriver() {
        System.out.println("executing Method com.pockerplayer.BaseTest.setupDriver() @BeforeClass");
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, TestData.waitTime);
        driver.get(TestData.serverAddress);
    }

    @AfterClass
    public void tearDown(){
        System.out.println("executing Method com.pockerplayer.BaseTest.tearDown() @AfterClass");
        sleep (1000);
        driver.quit();
    }

    private void sleep (int msecs){
        try {
            Thread.sleep(msecs);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
