package com.pockerplayer;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage{

    final WebDriver driver;
    final WebDriverWait wait;


    public BasePage(WebDriver driver, WebDriverWait wait){


        this.driver = driver;
        this.wait = wait;

        PageFactory.initElements(this.driver, this);
    }

    String getTitle(){
        return driver.getTitle();
    }



}
