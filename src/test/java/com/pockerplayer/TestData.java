package com.pockerplayer;

import java.util.ArrayList;

class TestData {

    static String serverAddress = "http://qa-poker.teaminternational.com";
    static String adminLogin = "admin";
    static String adminPassword = "test";
    static String adminLoginWrong = "user12";
    static String adminPasswordWrong = "123123";



    static int waitTime = 20;
    String messageSuccess = "Player have been added successfully";

    static ArrayList<PockerPlayer> getPlayers(){
        int number = (int) (Math.random() * 1000000000);
        String username = "Re" + number;
        ArrayList <PockerPlayer> testData = new ArrayList<PockerPlayer>(2);
        testData.add(new PockerPlayer(username, "QwE" + number, username + "@hosting.com",
                username + "_firstName", username + "_lastName", username + "_HomeCity",
                username + " Home Address in the City of Users",username + "Phone"));
        testData.add(new PockerPlayer(username, "AsD" + number, username + "@gmail.com",
                username + "_SimpleName",username + "_FamilyName", username + "_BestCity",
                username + " Home of User",username + "Cellphone"));
        return testData;
    }
}
