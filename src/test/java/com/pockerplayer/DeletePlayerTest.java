package com.pockerplayer;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.List;


public class DeletePlayerTest extends BaseTest{
    private PlayersListPage playersListPage;
    private PockerPlayer player;

    @BeforeMethod
    private void preconditions(){
        System.out.println("executing Method com.pockerplayer.DeletePlayerTest.preconditions() @BeforeMethod");
        player = TestData.getPlayers().get(0);
        playersListPage = new LoginPage(driver, wait).successfullLogin(TestData.adminLogin, TestData.adminPassword)
                .getInsertPlayerPage().fillFieldsAndSubmit(player);
    }

    @Test
    public void deletingPlayerTest(){
        System.out.println("executing Method com.pockerplayer.DeletePlayerTest.deletingPlayerTest() @Test");
        List<WebElement> result = playersListPage.getSearchResults(player);
        Assert.assertEquals(result.size(),1);
        playersListPage.deletePlayer(player);
        result = playersListPage.getSearchResults(player);
        Assert.assertTrue(result.isEmpty());
    }

    @AfterClass
    private void logOut(){
        System.out.println("executing Method com.pockerplayer.DeletePlayerTest.logOut() @AfterClass");
        playersListPage.logout();
    }
}
